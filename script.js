
const getData = () => {
fetch('https://jsonplaceholder.typicode.com/posts').then((response) => {
	/*console.log('resolved', response);*/
	return response.json();
}).then((data) => {
	console.log(data)
	let users = "";
	data.forEach((user) => {
		users += `<div>
		<h3>${user.name}</h3>
		<p>${user.email}</p>
		</div>`;
	});
document.querySelector('#output').innerHTML = users;

}).catch((err) => {
	console.log('rejected', err);
});
};

const btn1 = document.querySelector("#btn1");
btn1.addEventListener('click', getData);